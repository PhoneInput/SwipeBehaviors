# SwipeBehaviors

SwipeBehaviors is a collection of programs intended to be used in a pipeline to interpret swipes from a virtual keyboard. Programs should not require arguments, and may rely on other programs or files.

wvkbd is one such keyboard, implementing swipe output with the `-O` flag.

In SXMO, intended usage is by defining KEYBOARD_ARGS="-H 170 -O | swipeB", then copying or sym-linking your favorite behavior to `~/.local/bin/swipeB`. Then you can easily switch behavior by changing the link and relaunching the keyboard.
WARNING: With wvkbd before version v0.8, using a broken script can render your virtual keyboard broken. Make sure you have a way of recovery (e.g. SSH or ttyescape) before messing with scripts so you don't end up in a keyboard death-loop.

## Related Projects
* [wvkbd](https://github.com/jjsullivan5196/wvkbd): Virtual keyboard with swipe output mode.
* [swipeGuess](https://git.sr.ht/~earboxer/swipeGuess): Quickly query a swipe against a lexicon to get a number of possible words. (Plus some helper scripts for sorting/scoring.)
* [suggpicker](https://git.sr.ht/~earboxer/suggpicker): Simple suggestion picker based on wvkbd that operates like a pipe.
* [zig-input-method](https://git.sr.ht/~aren/zig-input-method): Suggestion picker written in zig and manager for correction and prediction.
* [clickclack](https://git.sr.ht/~proycon/clickclack): Haptic/audio feedback tool which may be chained before swipeB with the flags `-e -o`.
