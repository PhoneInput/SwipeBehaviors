#!/bin/sh
sock="$XDG_RUNTIME_DIR/sgp.sock"
words=~/.local/share/sxmo/words.txt
keymap=~/.local/share/sxmo/keyboard.map.tsv
rm "$sock"
mkfifo "$sock"
cat 0<> "$sock"|suggpicker 2>/dev/null|completelyTypeWord.sh &
swipeGuess "$words" 5 | while read -r word; do
	set -- $word
	if test -n "$2"; then
		s1="$(echo "$1" | mapScore "$keymap" bee|cut -f1)"
		s2="$(echo "$2" | mapScore "$keymap" bee |cut -f1)"
		cutoff="$(( ($s1 * 19 / 20) - 2 ))"
		if [ "$s2" -lt "$cutoff" ]; then
			set -- "$1"
		elif [ -n "$4" ]; then
			s4="$(echo "$4" | mapScore "$keymap" bee |cut -f1)"
			if [ "$s4" -lt "$cutoff" ]; then
				set -- "$1" "$2" "$3"
			fi
		fi
	fi
	if test "$1" && ! test "$2"; then
    wtype -P Backspace
		wtype "$1"
    wtype -P Space
	elif ! test "$2"; then
		echo "" >> "$sock"
	else
		echo "$word" >> "$sock"
	fi
done

killall suggpicker
