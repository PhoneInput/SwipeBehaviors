#!/bin/sh
sock="$XDG_RUNTIME_DIR/predictor.sock"
kl="$XDG_RUNTIME_DIR/keylog"
lw="$kl/pred.lastword"
tw="$kl/pred.tfw"
complete="$XDG_RUNTIME_DIR/pred.complete.bool"
words=~/.local/share/sxmo/words.txt
twow=~/.local/share/sxmo/count_2w.txt
rm -f "$sock" "$lw" "$complete"
mkdir -p "$kl"
chmod 700 "$kl"
mkfifo "$sock"
saveWord(){
	IFS=''
	while read -r word; do
		echo "$word"
		if echo "$word" |grep -q " "; then
			printf %s "$word" | cut -d' ' -f1 >> "$lw"
			printf " " >> "$lw"
			mv "$lw" "$tw"
			word="$(printf %s "$word"| cut -d' ' -f2| tee "$lw")"
		else
			printf %s "$word" >> "$lw"
			word="$(cat "$lw")"
		fi
		rm -f "$complete"
		cat "$words" "$twow" | quick5 "$word" >> "$sock"
	done
}
typeSomehow(){
	IFS=''
	while read -r word; do
		if test -e "$complete"; then
			test -n "${word#?}" && wl-ime-type -- "${word#?}"
		else
			wl-ime-type -- "$word"
		fi
	done
}
cat 0<> "$sock"|suggpicker 2>/dev/null|saveWord | typeSomehow &
IFS=''
while read -r swipe; do
	if test -n "${swipe#?}"; then
		touch "$complete"
		# TODO: change words
		echo "$swipe" | swipeGuess "$words" 5 >> "$sock"
	else
		if test "$swipe" = " "; then
			if test -e "$lw"; then
				printf " " >> "$lw"
				rm -f "$complete"
				cat "$twow" | quick5 "$(cat "$lw")" >> "$sock"
				mv "$lw" "$tw"
			fi
		elif test "$swipe" = "$(echo -e "\x08")"; then
			lastword="$(cat "$lw" 2>/dev/null)"
			printf %s "$lastword" | sed 's/.$//g' > "$lw"
		elif test "$swipe" = "$(echo -e "\n")"; then
			rm -f "$lw" "$tw"
		else
			printf %s "$swipe" >> "$lw"
		fi
	fi
done
killall suggpicker
