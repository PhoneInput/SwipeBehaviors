#include <stdio.h>
#include <stdbool.h>
#include <ctype.h>
#include <string.h>

#define BUFSIZE 1024
char wordBuff[BUFSIZE];

// fgets, but without the newline/tab.
char *fgetst(char *restrict s, int size, FILE *restrict stream) {
	char *r = fgets(s, size, stream);
	if (r) {
		char * sp = s;
		for(; sp[0] != '\0'; sp++) {
			if (sp[0] == '\n' || sp[0] == '\t') {
				sp[0] = '\0';
			}
		}
	}
	return r;
}

bool strdif(char* a, char* b) {
	for (;a[0];) {
		if (tolower(a[0]) != tolower(b[0])){
			return true;
		}
		a++;
		b++;
		if (!b[0])return true;
	}
	return false;
}

int main(int argc, char **argv) {
	if(argc!=2)return 1;
	int found = 0;
	size_t len = strlen(argv[1]);

	while (found < 5 && fgetst(wordBuff, BUFSIZE, stdin)) {
		if (!strdif(argv[1], wordBuff)) {
			if (found > 0) printf("\t");
			printf("%s", wordBuff+len);
			found++;
		}
	}
	printf("\n");
	return 0;
}
